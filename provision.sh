#!/bin/sh

pushd /home/vagrant > /dev/null
mkdir -p .ssh
chmod 700 .ssh
if [ -f ansible/.vagrant/machines/target/virtualbox/private_key ]; then
	cp ansible/.vagrant/machines/target/virtualbox/private_key .ssh/target_key
	chmod 600 .ssh/target_key
fi
chown -R vagrant:vagrant .ssh/

if [ ! $(which pip > /dev/null 2>&1) ]; then
	curl -kL https://bootstrap.pypa.io/get-pip.py | python
fi

pip install virtualenv > /dev/null

if [ ! -d py-env ]; then
	virtualenv py-env
fi
source py-env/bin/activate
pip install -r ansible/requirements/base.txt > /dev/null
popd > /dev/null
