#### ansible-books

このリポジトリはansibleの使い方を説明するために作成しています。

対象バージョンは以下の通りです。

- Ansible 2.4
- Python 2.7

仮想環境を作成するので、以下のソフトウェアが必要です。

- Virtualbox
- Vagrant

---

#### 手順

1. Vagrantfile.sampleをコピーして、Vagrantfileを作成
1. 既に192.168.33.30を使用している場合は、
Vagrantfileのホストオンリーアダプタとssh_configのHostNameを変更

1. プロキシ環境下の場合は、config.proxy.httpを明記

1. vagrant up で仮想環境を起動

1. 共有フォルダのマウントに失敗するので、vagrant vbguest でGuestAdditionsをインストール

1. vagrant reload && vagrant ssh ansible

1. source ~/py-env/bin/activate

1. cd ~/ansible/samples

1. ansible-playbook -i inventories/sample/hosts site.yml で環境構築

